
.. _le_hai_2023_10_11:

==========================================================================================================================================
 |LeHai| 2023-10-11 **"Nous vivrons": comment Joann Sfar a imaginé son dessin devenu viral, après l'attaque du Hamas contre Israël**
==========================================================================================================================================

- https://www.bfmtv.com/people/bandes-dessinees/nous-vivrons-comment-joann-sfar-a-imagine-son-dessin-devenu-viral-apres-l-attaque-du-hamas-contre-israel_AV-202310110371.html
- :ref:`antisem:pins_hay`


L'auteur du Chat du rabbin, raconte comment il a imaginé le dessin, en réaction 
à l'attaque du Hamas contre Israël samedi. Une image devenue virale.

"Cela veut dire 'nous vivrons'." Cette phrase, en légende d'une aquarelle de 
Joann Sfar représentant le mot hébraïque 'hai', qui signifie 'la vie', est 
devenue virale le week-end dernier sur Instagram, après l'attaque du Hamas 
contre Israël, qui a fait plus de 1.200 victimes selon un dernier bilan 
publié ce mercredi matin.

Avec près de 33.000 "likes" et autant de partages, ce visuel fort du dessinateur 
du Chat du Rabbin est devenu un des symboles du soutien aux victimes israéliennes. 

Interrogé par BFMTV sur ce qui l'a inspiré, Joann Sfar précise que "c'est tout simple":

Yoann Sfar

	"Quand (les juifs) trinquent, ils disent 'L'Chaim', ça veut dire 'à nos vies'. 
	
	Le 'haï', c'est ce qu'on met autour du cou des jeunes gens (juifs) pour 
	leur porter bonheur. 
	
	Je trouve très beau le fait que là où de nombreux hymnes nationaux appellent 
	au sang et au massacre, le slogan israélien est: 
	
	'Am Israël Haï!', c'est 'Israël vivra'."

Joann Sfar se défend de vouloir inciter à "choisir son camp" et souhaite au 
contraire faire "comprendre l'humanité et la complexité" d'une situation 
"forcément déchirante puisqu'en 1917 les Anglais ont promis aux Arabes et 
aux Juifs la même terre. 

C'est ça qui s'est passé lors de la déclaration Balfour."


Pas du cartoon mais de l'affiche
======================================

Le dessinateur, dont l'œuvre est exposée à partir de jeudi 12 octobre 2023 
au Musée d'art et d'histoire du Judaïsme, commente souvent l'actualité en dessin. 

En réaction à l'attentat contre Charlie Hebdo, il avait déjà imaginé en 2015 
un dessin lui aussi très relayé, qui synthétisait en une formule percutante 
la situation: "Si Dieu existe, il ne tue pas pour un dessin."


"Venant d'un petit pays qui a la taille des Alpes-Maritimes et qui peut être 
rayé de la carte en 24 heures, venant d'une population, les juifs, qui où 
qu'elle soit depuis l'antiquité a connu le risque de l'extermination, cette 
aspiration à vivre, elle suscite une rage qui est difficile à comprendre", 
commente encore Joann Sfar.

Pour lui, ce dessin entend lutter contre la désinformation qui règne depuis 
le début du conflit: "Les enfants ou les jeunes gens mal informés depuis la 
France s'imaginent qu'Israël est une terre expansionniste qui occupe tout 
le Proche-Orient. J'essaye d'amener les auditeurs ou les gens qui me lisent 
à s'intéresser à l'histoire réelle."

Joann Sfar

	"C'est marrant, parce qu'il y a une partie de moi qui n'aime pas faire ça", 
	réagit Joann Sfar. 
	
	"Quand on me parle d'un dessinateur politique et qu'on me dit, 'en un dessin, 
	on a tout compris à l'actualité', ça m'agace parce que je n'aime pas les 
	simplifications. 
	
	J'aime bien qu'on voie que le monde est complexe."


"Mais s'il y a un lieu où je me retrouve dans ce que vous dites, c'est presque 
plus (dans ce rôle) d'affichiste ou de graphiste. 

J'ai un travail qui fonctionne par images multiples, textes protéiformes, livres 
dans lesquels il y a de plus en plus de pages. 
Et quand je dois me résoudre à l'image unique, ce n'est pas du cartoon, mais 
plus de l'affiche."


Le dessinateur, dont le nouveau Chat du rabbin en librairies ce jeudi dénonce 
aussi l'antisémitisme ambiant, conclut: "Quand j'ai écrit ce 'hai', qui est 
une calligraphie, je l'ai vraiment fait comme un affichiste, en pensant aux gens 
qui faisaient les sérigraphies aux Beaux-Arts pendant Mai 68 ou à des choses comme ça."
